from unittest.mock import call, patch
import unittest.mock as mock
import pytest

from ligo import raven


class GResponse(object):
    def __init__(self, graceid, group):
        self.graceid = graceid
        self.group = group
    def json(self):
        return {"graceid": self.graceid,
                "group": self.group}

class MockGracedb(object):
    def __init__(self, service_url='https://gracedb-mock.org/api/'):
        self._service_url = service_url
        self._service_info = None

    def event(self, graceid):
        if graceid == 'G8':
            group = 'Burst'
        else:
            group = 'CBC'
        return GResponse(graceid, group)

    def superevents(self, args):
        arg_list = args.split('..')
        tl = float(arg_list[0])
        th = float(arg_list[1])
        results = []
        if tl <= 95.5 <= th:
            results.append({"superevent_id": "S1",
                            "t_0": 95.5,
                            "preferred_event": "G1"})
        if tl <= 96.5 <= th:
            results.append({"superevent_id": "S2",
                            "t_0": 96.5,
                            "preferred_event": "G2"})
        if tl <= 97.5 <= th:
            results.append({"superevent_id": "S3",
                            "t_0": 97.5,
                            "preferred_event": "G3"})
        if tl <= 98.5 <= th:
            results.append({"superevent_id": "S4",
                            "t_0": 98.5,
                            "preferred_event": "G4"})
        if tl <= 99.5 <= th:
            results.append({"superevent_id": "S5",
                            "t_0": 99.5,
                            "preferred_event": "G5"})
        if tl <= 100.5 <= th:
            results.append({"superevent_id": "S6",
                            "t_0": 100.5,
                            "preferred_event": "G6"})
        if tl <= 101.5 <= th:
            results.append({"superevent_id": "S7",
                            "t_0": 101.5,
                            "preferred_event": "G7"})
        if tl <= 102.5 <= th:
            results.append({"superevent_id": "S8",
                            "t_0": 102.5,
                            "preferred_event": "G8"})
        if tl <= 103.5 <= th:
            results.append({"superevent_id": "S9",
                            "t_0": 103.5,
                            "preferred_event": "G9"})
        if tl <= 104.5 <= th:
            results.append({"superevent_id": "S10",
                            "t_0": 104.5,
                            "preferred_event": "G10"})
        return results

    @mock.create_autospec
    def writeLog(self, gid, message, tag_name=[]):
        return gid, message, tag_name



def expected_results(event_type, gpstime, tl, th,
                 gracedb=None, group=None, pipelines=[]):
    if tl==-2 and (th==2 and group==None):
        return [{"superevent_id": "S4",
                 "t_0":98.5,
                 "preferred_event": "G4"},
                {"superevent_id": "S5",
                 "t_0": 99.5,
                 "preferred_event": "G5"},
                {"superevent_id": "S6",
                 "t_0": 100.5,
                 "preferred_event": "G6"},
                {"superevent_id": "S7",
                 "t_0": 101.5,
                 "preferred_event": "G7"}]
    elif tl==-5 and (th==1 and group==None):
        return [{"superevent_id": "S1",
                 "t_0":95.5,
                 "preferred_event": "G1"},
                {"superevent_id": "S2",
                 "t_0":96.5,
                 "preferred_event": "G2"},
                {"superevent_id": "S3",
                 "t_0":97.5,
                 "preferred_event": "G3"},
                {"superevent_id": "S4",
                 "t_0":98.5,
                 "preferred_event": "G4"},
                {"superevent_id": "S5",
                 "t_0": 99.5,
                 "preferred_event": "G5"},
                {"superevent_id": "S6",
                 "t_0": 100.5,
                 "preferred_event": "G6"}]
    elif tl==2 and th==5:
        return [{"superevent_id": "S8",
                 "t_0": 102.5,
                 "preferred_event": "G8"},
                {"superevent_id": "S9",
                 "t_0": 103.5,
                 "preferred_event": "G9"},
                {"superevent_id": "S10",
                 "t_0": 104.5,
                 "preferred_event": "G10"}]
    elif group=='Burst':
        return [{"superevent_id": "S8",
                 "t_0": 102.5,
                 "preferred_event": "G8"}]
    else:
        return []  


@pytest.mark.parametrize(
    'gracedb_id, event_type,gpstime,tl,th, group',
    [['E1','Superevent', 100, -2, 2, None],
     ['E2','Superevent', 100, -5, 1, None],
     ['E3','Superevent', 100,  2, 5, None],
     ['E4','Superevent', 100, -5, 5, 'Burst'],
     ['E5','Superevent', 150, -10, 10, None]])
def test_search_return(monkeypatch,
                       gracedb_id, event_type, gpstime, tl, th, group):

    class ExtTrig(object):
        def __init__(self, gracedb_id):
            self.graceid = gracedb_id
            self.neighbor_type = 'S'
            self.gracedb = MockGracedb()
            self.gpstime = gpstime

    event = ExtTrig(gracedb_id)

    results = raven.search.search(event, tl, th, gracedb=MockGracedb(),
                                  group=group, pipelines=[])
    assert results == expected_results(event_type, gpstime, tl, th, group=group)

