from unittest.mock import call, patch
import unittest.mock as mock
import pytest
from astropy.utils.data import get_file_contents
from math import isclose

from ligo.gracedb import rest
from ligo import raven

class G1Response(object):

    def json(self):
        return {"graceid": "G1",
                "group": "CBC"}

class G2Response(object):

    def json(self):
        return {"graceid": "G2",
                "group": "Burst"}

class G3Response(object):

    def json(self):
        return {"graceid": "G3",
                "group": "CBC"}

class S100Response(object):

    def json():
        return {"superevent_id": "S100",
                "far": 1e-5,
                "t_0": 100,
                "preferred_event": "G1"}
    
class S101Response(object):

    def json():
        return {"superevent_id": "S101",
                "far": 1e-7,
                "t_0": 100,
                "preferred_event": "G2"}

class S102Response(object):

    def json():
        return {"superevent_id": "S102",
                "far": 1e-9,
                "t_0": 100,
                "preferred_event": "G3"}


class MockGracedb(object):
    def __init__(self, service_url='https://gracedb-mock.org/api/'):
        self._service_url = service_url
        self._service_info = None

    def events(self, args):
        arg_list = args.split(' ')
        event_type = arg_list[0]
        tl = float(arg_list[1])
        th = float(arg_list[3])
        results = []
        if event_type == 'External':
            if tl <= 102 <= th:
                results.append({"graceid": "E1",
                                "gpstime": 102.0,
                                "pipeline": "SWIFT",
                                "group": "External",
                                "search": "GRB"})
            if tl <= 106 <= th :
                results.append({"graceid": "E2",
                                "gpstime": 106.0,
                                "pipeline": "SNEWS",
                                "group": "External",
                                "search": "Supernova"})
            if tl <= 115 <= th:
                results.append({"graceid": "E3",
                                "gpstime": 115.0,
                                "pipeline": "Fermi",
                                "group": "External",
                                "search": "SubGRB"})
            if tl <= 99.5 <= th:
                results.append({"graceid": "E4",
                                "gpstime": 99.5,
                                "pipeline": "Fermi",
                                "group": "External",
                                "search": "SubGRB"})
        return results

    def event(self, graceid):
        if graceid == 'G1':
            return G1Response()
        if graceid == 'G2':
            return G2Response()
        if graceid == 'G3':
            return G3Response()

    def superevents(self, args):
        arg_list = args.split('..')
        tl = float(arg_list[0])
        th = float(arg_list[1])
        results = []
        if tl <= 100.5 <= th:
            results.append({"superevent_id": "S1",
                            "t_0": 100.5,
                            "preferred_event": "G1"})
        if tl <= 96 <= th:
            results.append({"superevent_id": "S2",
                            "t_0": 96.0,
                            "preferred_event": "G2"})
        if tl <= 106 <= th:
            results.append({"superevent_id": "S3",
                            "t_0": 106.0,
                            "preferred_event": "G3"})
        return results

    def superevent(self, graceid):
        if graceid == 'S100':
            return S100Response
        if graceid == 'S101':
            return S101Response
        if graceid == 'S102':
            return S102Response

    @mock.create_autospec
    def writeLog(self, gid, message, tag_name=[]):
        return gid, message, tag_name

    def _service_url(self):
        return 'https://gracedb-mock.org/api/T100'


def query_return(event_type, gpstime, tl, th,
                 gracedb=None, group=None, pipelines=None,
                 searches=None):
    if tl==-5 and (group==None and pipelines==None):
        return [{"superevent_id": "S1",
                 "t_0":100.5,
                 "preferred_event": "G1"},
                {"superevent_id": "S2",
                 "t_0": 96.0,
                 "preferred_event": "G2"}]
    elif tl==-600 and group=='Burst':
        return [{"superevent_id": "S2",
                 "t_0": 96.0,
                 "preferred_event": "G2"}]
    elif tl==-5 and group=='CBC':
        return [{"superevent_id": "S1",
                 "t_0": 100.5,
                 "preferred_event": "G1"}]
    elif (tl==-1 and searches) and ('SubGRB' in searches):
        return [{"graceid": "E4",
                 "gpstime": 99.5,
                 "pipeline": "Fermi",
                 "group": "External",
                 "search": "SubGRB"}]
    elif tl==-1 and pipelines==['Fermi','SWIFT']:
        return [{"graceid": "E1",
                 "gpstime": 102.0,
                 "pipeline": "SWIFT",
                 "group": "External",
                 "search": "GRB"},
                {"graceid": "E4",
                 "gpstime": 99.5,
                 "pipeline": "Fermi",
                 "group": "External",
                 "search": "SubGRB"}]
    elif tl==-10 and th==10:
        return [{"graceid": "E2",
                 "gpstime": 106.0,
                 "pipeline": "SNEWS",
                 "group": "External",
                 "search": "Supernova"}]
    else:
        return []  


@pytest.mark.parametrize(
    'gracedb_id,event_type,gpstime,tl,th,group,pipelines,searches',
    [['E100','Superevent', 100, -5, 1, None, None, None],
     ['E101','Superevent', 100, -600, 60, 'Burst', None, None],
     ['E102','Superevent', 100, -5, 1, 'CBC', None, None],
     ['S100','External', 100, -1, 5, None, ['Fermi','SWIFT'], None],
     ['S100','External', 100, -1, 5, None, ['Fermi','SWIFT'], ['SubGRB']],
     ['S100','External', 100, -1, 5, None, ['Fermi','SWIFT'], ['SubGRB','SubGRBTargeted']],
     ['S101','External', 100, -10, 10, None, ['SNEWS'], None]])
def test_call_query(gracedb_id, event_type, gpstime, tl, th, group, pipelines,
                    searches):
 
    results = raven.search.query(
                  event_type, gpstime, tl, th, gracedb=MockGracedb(),
                  group=group, pipelines=pipelines, searches=searches)
  
    assert results == query_return(event_type, gpstime, tl, th, group=group,
                                   pipelines=pipelines, searches=searches)
 

@pytest.mark.parametrize(
    'gracedb_id,event_type,gpstime,tl,th,group,pipelines,searches',
    [['E100','Superevent', 100, -5, 1, None, None, None],
     ['E101','Superevent', 100, -600, 60, 'Burst', None, None],
     ['E102','Superevent', 100, -5, 1, 'CBC', None, None],
     ['S100','External', 100, -1, 5, None, ['Fermi','SWIFT'], None],
     ['S100','External', 100, -1, 5, None, ['Fermi','SWIFT'], ['SubGRB']],
     ['S101','External', 100, -10, 10, None, ['SNEWS'], None]])
def test_search_return(gracedb_id, event_type, gpstime, tl, th, group, pipelines,
                       searches):

    class SE(object):
        def __init__(self, gracedb_id):
            self.graceid = gracedb_id
            self.neighbor_type = 'E'
            self.gracedb = MockGracedb()
            self.gpstime = gpstime
    class ExtTrig(object):
        def __init__(self, gracedb_id):
            self.graceid = gracedb_id
            self.neighbor_type = 'S'
            self.gracedb = MockGracedb()
            self.gpstime = gpstime

    if gracedb_id.startswith('S'):
        event = SE(gracedb_id)
    else:
        event = ExtTrig(gracedb_id)

    results = raven.search.search(event, tl, th, gracedb=MockGracedb(),
                                  group=group, pipelines=pipelines, searches=searches)
    assert results == query_return(event_type, gpstime, tl, th, group=group,
                                   pipelines=pipelines, searches=searches)

    calls_list = MockGracedb.writeLog.call_args_list

    if gracedb_id=='E100':
        assert calls_list[0][0][1] == "E100"
        assert calls_list[0][0][2] == "RAVEN: Superevent candidate <a href='https://gracedb-mock.org/superevents/S1'>S1</a> within [-5, +1] seconds, about 0.500 second(s) after External event"
        assert calls_list[1][0][1] == "S1"
        assert calls_list[1][0][2] == "RAVEN: External event <a href='https://gracedb-mock.org/events/E100'>E100</a> within [-1, +5] seconds, about 0.500 second(s) before Superevent"
        assert calls_list[2][0][1] == "E100"
        assert calls_list[2][0][2] == "RAVEN: Superevent candidate <a href='https://gracedb-mock.org/superevents/S2'>S2</a> within [-5, +1] seconds, about 4.000 second(s) before External event"
        assert calls_list[3][0][1] == "S2"
        assert calls_list[3][0][2] == "RAVEN: External event <a href='https://gracedb-mock.org/events/E100'>E100</a> within [-1, +5] seconds, about 4.000 second(s) after Superevent"
    elif gracedb_id=='E101':
        assert calls_list[4][0][1] == "E101"
        assert calls_list[4][0][2] == "RAVEN: Superevent Burst candidate <a href='https://gracedb-mock.org/superevents/S2'>S2</a> within [-600, +60] seconds, about 4.000 second(s) before External event"
        assert calls_list[5][0][1] == "S2"
        assert calls_list[5][0][2] == "RAVEN: External event <a href='https://gracedb-mock.org/events/E101'>E101</a> within [-60, +600] seconds, about 4.000 second(s) after Superevent"
    elif gracedb_id=='E102':
        assert calls_list[6][0][1] == "E102"
        assert calls_list[6][0][2] == "RAVEN: Superevent CBC candidate <a href='https://gracedb-mock.org/superevents/S1'>S1</a> within [-5, +1] seconds, about 0.500 second(s) after External event"
        assert calls_list[7][0][1] == "S1"
        assert calls_list[7][0][2] == "RAVEN: External event <a href='https://gracedb-mock.org/events/E102'>E102</a> within [-1, +5] seconds, about 0.500 second(s) before Superevent"
    elif gracedb_id=='S100' and not searches:
        assert calls_list[8][0][1] == "E1"
        assert calls_list[8][0][2] == "RAVEN: Superevent candidate <a href='https://gracedb-mock.org/superevents/S100'>S100</a> within [-5, +1] seconds, about 2.000 second(s) before External event"
        assert calls_list[9][0][1] == "S100"
        assert calls_list[9][0][2] == "RAVEN: External event ['Fermi', 'SWIFT'] <a href='https://gracedb-mock.org/events/E1'>E1</a> within [-1, +5] seconds, about 2.000 second(s) after Superevent"
        assert calls_list[10][0][1] == "E4"
        assert calls_list[10][0][2] == "RAVEN: Superevent candidate <a href='https://gracedb-mock.org/superevents/S100'>S100</a> within [-5, +1] seconds, about 0.500 second(s) after External event"
        assert calls_list[11][0][1] == "S100"
        assert calls_list[11][0][2] == "RAVEN: External event ['Fermi', 'SWIFT'] <a href='https://gracedb-mock.org/events/E4'>E4</a> within [-1, +5] seconds, about 0.500 second(s) before Superevent"
    elif gracedb_id=='S100' and searches:
        assert calls_list[12][0][1] == "E4"
        assert calls_list[12][0][2] == "RAVEN: Superevent candidate <a href='https://gracedb-mock.org/superevents/S100'>S100</a> within [-5, +1] seconds, about 0.500 second(s) after External event"
        assert calls_list[13][0][1] == "S100"
        assert calls_list[13][0][2] == "RAVEN: External event ['Fermi', 'SWIFT'] ['SubGRB'] <a href='https://gracedb-mock.org/events/E4'>E4</a> within [-1, +5] seconds, about 0.500 second(s) before Superevent"
    elif gracedb_id=='S101':
        assert calls_list[14][0][1] == "E2"
        assert calls_list[14][0][2] == "RAVEN: Superevent candidate <a href='https://gracedb-mock.org/superevents/S101'>S101</a> within [-10, +10] seconds, about 6.000 second(s) before External event"
        assert calls_list[15][0][1] == "S101"
        assert calls_list[15][0][2] == "RAVEN: External event ['SNEWS'] <a href='https://gracedb-mock.org/events/E2'>E2</a> within [-10, +10] seconds, about 6.000 second(s) after Superevent"


class EResponse(object):
    def __init__(self, graceid):
        self.graceid = graceid
        self.group = "External"
        if graceid=='E1':
            self.pipeline = "SWIFT"
            self.gpstime = 102.0
            self.search = "GRB"
        if graceid=='E2':
            self.pipeline = "SNEWS"
            self.gpstime = 106.0
            self.search = "Supernova"
        if graceid=='E3':
            self.pipeline = "Fermi"
            self.gpstime = 115.0
            self.search = "SubGRB"
        if graceid=='E4':
            self.pipeline = "Swift"
            self.gpstime = 115.0
            self.search = "SubGRB"
 
    def json(self):
        return {"graceid": self.graceid,
                "group": self.group,
                "pipeline": self.pipeline,
                "gpstime": self.gpstime,
                "search": self.search}


class MockGracedb2(object):
    def __init__(self, service_url='https://gracedb-mock.org/api/'):
        self._service_url = service_url
        self._service_info = None

    def superevent(self, graceid):
        if graceid == 'S100':
            return S100Response
        if graceid == 'S101':
            return S101Response
        if graceid == 'S102':
            return S102Response

    def event(self, graceid):
        return EResponse(graceid)

    def files(self, graceid, filename, raw=True):
        if graceid=='S100' and filename=='bayestar.fits.gz':
            return S100Skymap()
        if graceid=='E1' and filename=='glg_healpix_all_bn_v00.fit':
            return E1Skymap()
        else:
            raise ValueError

    @mock.create_autospec
    def writeLog(self, gid, message, filename, contents, tag_name=[]):
        return gid, message, tag_name


@patch('ligo.raven.gracedb_events.SE')
@patch('ligo.raven.gracedb_events.ExtTrig')
def test_coinc_far_grb(mock_ExtTrig, mock_SE):

    result = raven.search.coinc_far('S100', 'E1', -1, 5, gracedb=MockGracedb2())
    assert isclose(result['temporal_coinc_far'], 5.8980e-10, abs_tol=1e-13)
    assert result['preferred_event'] == 'G1' 


@patch('ligo.raven.gracedb_events.SE')
@patch('ligo.raven.gracedb_events.ExtTrig')
def test_coinc_far_snews(mock_ExtTrig, mock_SE):

    result = raven.search.coinc_far('S101', 'E2', -10, 10, gracedb=MockGracedb2(),
                                    grb_search='Supernova')
    assert result == "RAVEN: WARNING: Invalid search. RAVEN only considers 'GRB', 'SubGRB', and 'SubGRBTargeted'."


@patch('ligo.raven.gracedb_events.SE')
@patch('ligo.raven.gracedb_events.ExtTrig')
def test_coinc_far_subgrb(mock_ExtTrig, mock_SE):

    result = raven.search.coinc_far('S102', 'E3', -5, 1, gracedb=MockGracedb2(),
                                    grb_search='SubGRB')
    assert isclose(result['temporal_coinc_far'], 7.1347e-14, abs_tol=1e-17)
    assert result['preferred_event'] == 'G3'


@patch('ligo.raven.gracedb_events.SE')
@patch('ligo.raven.gracedb_events.ExtTrig')
def test_coinc_far_swift_subgrb(mock_ExtTrig, mock_SE):

    result = raven.search.coinc_far('S101', 'E4', -30, 30, gracedb=MockGracedb2(),
                                    far_grb=1e-4, grb_search='SubGRBTargeted')
    assert isclose(result['temporal_coinc_far'], 5.2482e-9, abs_tol=1e-13)
    assert result['preferred_event'] == 'G2'


class S100Skymap(object):
    def read(self):
        return get_file_contents('ligo/tests/data/GW170817/bayestar.fits.gz',
                                 encoding='binary', cache=False)


class E1Skymap(object):
    def read(self):
        return get_file_contents('ligo/tests/data/GW170817/glg_healpix_all_bn_v00.fit',
                                 encoding='binary', cache=False)


def test_coinc_far_skymap():

    result = raven.search.coinc_far('S100', 'E1', -5, 1, gracedb=MockGracedb2(),
                                    grb_search='GRB', incl_sky=True, se_fitsfile='bayestar.fits.gz')

    assert isclose(result['spatiotemporal_coinc_far'], 5.6755e-11, abs_tol=1e-14)
    assert result['preferred_event'] == 'G1'


def test_calc_signif_gracedb():

    result = raven.search.calc_signif_gracedb('S100', 'E1', -5, 1, gracedb=MockGracedb2(),
                                              grb_search='GRB', incl_sky=True, se_fitsfile='bayestar.fits.gz')

    calls_list =  MockGracedb2.writeLog.call_args_list[0][0]
    call_tag = MockGracedb2.writeLog.call_args_list[0][1]

    assert calls_list[1] == 'S100'
    assert calls_list[2] == "RAVEN: Computed coincident FAR(s) in Hz with external trigger <a href='https://gracedb-mock.org/events/E1'>E1</a>"
    assert calls_list[3] == 'coincidence_far.json'
    assert eval(calls_list[4])['preferred_event'] == "G1"
    assert isclose(float(result['temporal_coinc_far']), 5.8980e-10, abs_tol=1e-14)
    assert isclose(float(result['spatiotemporal_coinc_far']), 5.6755e-11, abs_tol=1e-15)
    assert call_tag == {'tag_name': ['ext_coinc']}


