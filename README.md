Welcome to RAVEN, the Rapid, on-source VOEvent Coincidence Monitor.

More information can be found at the review documentation website:
   https://www.lsc-group.phys.uwm.edu/ligovirgo/cbcnote/GRBreview/RAVEN

A version of this documentation is included as a PDF file in the ```docs/``` subdirectory.

To install RAVEN locally:

```python setup.py install --prefix=/path/to/install/directory```

To run the RAVEN unit tests:

```python setup.py test```

These unittests are based on tests approved by the RAVEN review committee located here:

https://git.ligo.org/brandon.piotrzkowski/ravenreview

Finally, the most current RAVEN development version is always available from the
git repository:

```git clone git@git.ligo.org:lscsoft/raven.git```

This will require a valid SSH key with git.ligo.org; see https://git.ligo.org/profile/keys
for more information.
